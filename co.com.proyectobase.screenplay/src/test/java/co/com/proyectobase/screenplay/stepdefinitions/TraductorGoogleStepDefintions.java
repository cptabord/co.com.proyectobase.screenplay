package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefintions {
	
	@Managed(driver= "chrome")
	private WebDriver hisBrowser;
	private Actor rafa = Actor.named("Rafa");
	
	@Before
	public void configuracionInicial()
	{
		rafa.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^That Rafa want to use Google Traslate$")
	public void thatRafaWantToUseGoogleTraslate() throws Exception {
	    
		rafa.wasAbleTo(Abrir.LaPaginaDeGoogle());
	   
	}

	@When("^he translate the word (.*) form Ingles to Spanish$")
	public void heTranslateTheWordTableFormInglesToSpanish(String palabra) throws Exception {
		rafa.attemptsTo(Traducir.DeInglesAEspanolLa (palabra));
	    
	    
	}

	@Then("^he should see the word (.*) on the screen$")
	public void heShouldSeeTheWordTableOnTheScreen(String palabraEsperada) {
		rafa.should(seeThat(LaRespuesta.es(), equalTo(palabraEsperada)));
	    
	    
	}

}
