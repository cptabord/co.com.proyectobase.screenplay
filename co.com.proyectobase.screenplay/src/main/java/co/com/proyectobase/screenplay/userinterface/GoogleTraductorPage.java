package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class GoogleTraductorPage {

	public static final Target BOTON_LENGUAJE_ORIGEN = Target.the("Botón del idioma origen")
			.located(By.id("gt-sl-gms"));
	public static final Target BOTON_LENGUAJE_DESTINO = Target.the("Botón del idioma destino")
			.located(By.id("gt-tl-gms"));
	public static final Target OPCION_INGLES = Target.the("La opción ingles")
			.located(By.id(":1e"));
	public static final Target OPCION_ESPANOL = Target.the("El segundo idioma")
			.located(By.id(":3q"));
	public static final Target AREA_DE_TRADUCCION = Target.the("El lugar donde se escriben as palabras a traducir")
			.located(By.id("source"));
	public static final Target BOTON_TRADUCIR = Target.the("El botón de traducir")
			.located(By.id("gt-submit"));
	public static final Target AREA_TRADUCIDA = Target.the("El area donde ta se presenta la palabra traducida")
			.located(By.id("gt-res-dir-ctr"));
			
}
